import { useSelector, useDispatch } from 'react-redux';
import { pm } from '../../layers/turtleos-pm/pm';

// Load Platform configs
import { PLATFORM as DESKTOP } from '../../layers/platforms/turtleos-desktop/platform';
import { PLATFORM as MOBILE } from  '../../layers/platforms/turtleos-mobile/platform';

const platforms = {
    desktop: DESKTOP,
    mobile: MOBILE
};

const VALIDPLATFORMS = [
    ...Object.keys(platforms),
    'auto'
];

function writePlatform ( platform ) {
    if(VALIDPLATFORMS.indexOf(platform)===-1) throw new Error('Platform not found');
    const fs = pm.getState().vfs;
    try {
        let d = fs.readFileSync('/buildconf');
        if(d.error) throw new Error(d);
        let json = JSON.parse(d.data.content);
        json.platform=platform;
        d= fs.writeFileSync('/buildconf', JSON.stringify(json));
        if(d.error) throw new Error(d);
    } catch(e) {
        throw new Error('Failed to write file');
    }
}

function usePlatform ( platform ) {
    const dispatch = useDispatch();
    if(Object.keys(platforms).indexOf(platform)===-1) platform = Object.keys(platforms)[0];
    const plat = useSelector(state=>state.platform);
    const set = (plat) => {
        dispatch({
            type: 'platform:update',
            payload: {
                platform: plat
            }
        });
    };
    try {
    writePlatform(platform);
        set(platform);
    } catch(e) {};
    return [plat, set];
}

function detectPlatform () {
    let platform = Object.keys(platforms)[0];
    let dimens = getWindowDimensions();
    for(let [pf, {expects}] of Object.entries(platforms)) {
        if(expects(dimens.width, dimens.height)) {
            return pf;
        }
    }
    return platform;
}

function getWindowDimensions () {
    return {
        width: Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0),
        height: Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)
    };
}

function Platform(platform) {
    if(Object.keys(platforms).indexOf(platform)===-1) {
        return {};
    }
    return {
        getBootView: () => {
            return platforms[platform].render();
        },
        getWindow({dimens, view, appid, appname, icon, bridge, appdk}) {
            return platforms[platform].window({dimens:dimens, view:view, key:appid, appid:appid, appname:appname, icon:icon, bridge: bridge, appdk: appdk});
        }
    };
}

export {
    usePlatform,
    detectPlatform,
    Platform,
    getWindowDimensions,
    VALIDPLATFORMS,
    writePlatform
}
